var http = require('https');
var foo = require('./lib/mailer.js');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

  setInterval(function() {

    var options = {
      host: 'burghquayregistrationoffice.inis.gov.ie',
      port: 443,
      path: '/Website/AMSREG/AMSRegWeb.nsf/getAppsNear?openpage&cat=Work&sbcat=All&typ=New',
      method: 'POST'
    };

    http.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        var result = JSON.parse(chunk);
        var empty = result.empty;
        if (empty != 'TRUE')
        {
          foo.send(JSON.stringify(result));
          console.log('The website returned some timeslots. (at ' + new Date().toTimeString() + ').');
          console.log('The GNIB service returned the following content: ' + JSON.stringify(result));
        }
        else
        {
          console.log('No timeslots available (at ' + new Date().toTimeString() + ').');
          //console.log('The GNIB service returned the following content: ' + JSON.stringify(result));
        }
      });
    }).end();
  }, 5000);
